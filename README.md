# Freecodecamp projects

## 1. Responsive Web Design Projects

### 1.1. Build a Tribute Page

[Exercise](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-tribute-page)

[Design](https://images.template.net/116/Free-Professional-Resume-Template-440x570-1.jpg)

[Finished page on Codepen.io](https://codepen.io/zivatar/full/KKPoYze)

### 1.2. Build a Survey Form

[Exercise](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-survey-form)

[Design](https://surveyanyplace.com/wp-content/uploads/order-form1.jpg)

[Finished page on Codepen.io](https://codepen.io/zivatar/full/pozVymb)

### 1.3. Build a Product Landing Page

[Exercise](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-product-landing-page)

[Design](https://mk0zofoqaluvgdskgvsb.kinstacdn.com/photos/indochino-full.jpg)

[Page on Codepen.io](https://codepen.io/zivatar/full/XWrYrpm)